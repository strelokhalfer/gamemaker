#define Menu
/// argument0 = new_item

if is_string(argument0) Menu_select_slug(argument0);
else Menu_select_index(argument0);

#define Menu_select_index
/// argument0 = new_item

if argument0==current_item exit;
prev_item=current_item;
current_item=argument0;
event_user(0);

#define Menu_select_slug
/// argument0 = slug
var i, length=array_length_1d(menu_items);
for (i=0; i<length; i++)
{
    if menu_items[i].slug==argument0
    {
        Menu_select_index(i);
        exit;
    }
}