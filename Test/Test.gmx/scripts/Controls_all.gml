/// argument0 - controls map, argument1 - check for true or false

var length=ds_map_size(argument0);
var key=ds_map_find_first(argument0);
for (var i=1; i<=length; i++)
{
    if keyboard_check(ds_map_find_value(argument0, key))!=argument1 return false;
    key=ds_map_find_next(argument0, key);
}
return true;
