/// argument0 = Coord Plane instance id

var r=sqrt(power(x, 2)+power(y, 2));
var ang=arccos(x/r);
return r*sin(ang-argument0.image_angle)-argument0.y;
