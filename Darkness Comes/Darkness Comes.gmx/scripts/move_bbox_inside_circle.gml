#define move_bbox_inside_circle
var target=argument0, circle=argument1;

if !is_point_inside_circle(target.bbox_left, target.bbox_top, circle) move_point_inside_circle(target, target.bbox_left, target.bbox_top, circle);
if !is_point_inside_circle(target.bbox_left, target.bbox_bottom, circle) move_point_inside_circle(target, target.bbox_left, target.bbox_bottom, circle);
if !is_point_inside_circle(target.bbox_right, target.bbox_top, circle) move_point_inside_circle(target, target.bbox_right, target.bbox_top, circle);
if !is_point_inside_circle(target.bbox_right, target.bbox_bottom, circle) move_point_inside_circle(target, target.bbox_right, target.bbox_bottom, circle);

#define move_point_inside_circle
var target=argument0, point_x=argument1, point_y=argument2, circle=argument3;

var angle=point_direction(circle.x, circle.y, point_x, point_y);
var move_distance=point_distance(point_x, point_y, circle.x, circle.y)-circle.r;
target.x+=move_distance*dcos(angle-180);
target.y-=move_distance*dsin(angle-180);