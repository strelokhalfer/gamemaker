#define play_music
var music=argument0;
if music==-1 exit;
stop_music();
audio_play_sound(music, 1, false);
audio_sound_gain(music, global.music_volume, 0);

#define stop_music
audio_group_stop_all(Music);