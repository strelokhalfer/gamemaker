#define load_dictionary
var lang_id=argument0;
load_dictionary_by_code(global.languages[lang_id]);

#define load_dictionary_by_code
var lang_code=argument0;
var filename=working_directory+'\Localization\lang_'+lang_code+'.dic';
if not file_exists(filename)
{
    show_error('No dictionary for '+lang_code+'!', false);
    global.language=0;
    lang_code='en';
}

ds_map_clear(global.dictionary);
var file=file_text_open_read(filename);
while not file_text_eof(file)
{
    var line=file_text_readln(file);
    if line=='' continue;
    var assignment=string_pos('=', line);
    ds_map_add(global.dictionary, string_copy(line, 1, assignment-1), string_copy(line, assignment+1, string_length(line)-assignment-2));
}
file_text_close(file);