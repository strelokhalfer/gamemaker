var sfx=argument0, volume=argument1;
if sfx==-1 exit;
if !audio_is_playing(sfx) audio_play_sound(sfx, 1, false);
audio_sound_gain(sfx, global.sfx_volume*volume, 0);
