var monster=argument0;
var code=monster.code;
var current_count=ds_map_find_value(Stats.monster_stats, code);
if is_undefined(current_count) ds_map_add(Stats.monster_stats, code, 1);
else ds_map_add(Stats.monster_stats, code, current_count+1);
Stats.monster_kills++;
