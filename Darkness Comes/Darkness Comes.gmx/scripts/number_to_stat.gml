var number=argument0;
if number>1000000 return string(round(number/1000000))+'M';
if number>1000 return string(round(number/1000))+'K';
return string(round(number));
